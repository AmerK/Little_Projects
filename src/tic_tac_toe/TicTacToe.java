package tic_tac_toe;

//Simple "tic tac toe" game using one class and only static methods

import java.util.Scanner;

public class TicTacToe {

    static int row;
    static int column;
    public static final String RESET_COLOR = "\u001B[0m";
    public static final String RED_COLOR = "\u001B[31m";
    public static final String BLUE_COLOR = "\u001B[34m";
    public static final String YELLOW_COLOR = "\u001B[33m";

    public static void main(String[] args) {

        int size = 3;
        String[][] gameBoard = new String[size][size];
        int numberOfMoves = 0;
        String symbol = "X";

        System.out.println();

        printTheGameBoard(gameBoard);

        System.out.println();

        while (numberOfMoves < size*size)
        {
            System.out.println(YELLOW_COLOR + symbol + RESET_COLOR + BLUE_COLOR + " it's your turn. Choose coordinates with number 1-3 " +
                    "separated with comma (ex. 1,1): "
                    + RESET_COLOR);
            String coordinates = new Scanner(System.in).next();

            if (!checkCoordinates(coordinates))
            {
                System.out.println(RED_COLOR + "Wrong coordinates. There should be two numbers separated with a comma " +
                        "without space (ex. 1,1). Try again");
                continue;
            }
            else
            {
                splittingCoordinates(coordinates);
                if (!checkCoordinates())
                {
                    System.out.println(RED_COLOR + "Wrong coordinates. Coordinates must be higher than 0 and lower than 4." +
                            " Please try again");
                    continue;
                }
            }

            if (checkIfEmpty(gameBoard,row,column))
            {
                putSymbolOnBoard(gameBoard,row,column,symbol);
            }
            else
            {
                System.out.println(RED_COLOR + "This spot is already occupied! Try another one!");
                continue;
            }

            System.out.println();
            printTheGameBoard(gameBoard);
            System.out.println();

            if(someoneWon(gameBoard))
            {
                System.out.println(BLUE_COLOR + "Bravo " + YELLOW_COLOR + symbol + BLUE_COLOR + "! You WON!"
                        + RESET_COLOR);
                break;
            }

            symbol = changeSymbol(symbol);
            ++numberOfMoves;

            if (numberOfMoves == size*size)
            {
                System.out.println(BLUE_COLOR + "No one won! DRAW!" + RESET_COLOR);
            }
        }
    }

    public static boolean someoneWon(String[][] gameBoard)
    {
        boolean won = false;

        if (horizontalWon(gameBoard) || verticalWon(gameBoard) || diagonalWon(gameBoard))
        {
            won = true;
        }

        return won;
    }

    public static boolean horizontalWon(String[][] gameBoard)
    {
//                X|X|X
//                - - -
//                 | |
//                - - -
//                X|X|X

        boolean won = false;

        for (int i = 0; i < gameBoard.length; i++)
        {
            for (int j = 0; j < 1; j++)
            {
                if (gameBoard[i][j] != null)
                {
                    if (gameBoard[i][j].equals(gameBoard[i][j + 1]) && gameBoard[i][j].equals(gameBoard[i][j + 2]))
                    {
                        won = true;
                    }
                }
            }
        }
        return won;
    }

    public static boolean verticalWon(String[][] gameBoard)
    {
//                X| |X
//                - - -
//                X| |X
//                - - -
//                X| |X
        boolean won = false;

        for (int i = 0; i < gameBoard.length; i++)
        {
            for (int j = 0; j < 1; j++)
            {
                if (gameBoard[j][i] != null)
                {
                    if (gameBoard[j][i].equals(gameBoard[j + 1][i]) && gameBoard[j][i].equals(gameBoard[j + 2][i]))
                    {
                        won = true;
                    }
                }
            }
        }
        return won;
    }

    public static boolean diagonalWon(String[][] gameBoard)
    {
//                X| |X
//                - - -
//                 |X|
//                - - -
//                X| |X
        boolean won = false;

        for (int i = 0; i < 1; i++)
        {
            for (int j = 0; j < 1; j++)
            {
                if (gameBoard[i][j] != null)
                {
                    if (gameBoard[i][j].equals(gameBoard[i + 1][j + 1]) && gameBoard[i][j].equals(gameBoard[i + 2][j + 2]))
                    {
                        won = true;
                    }
                }
                if (gameBoard[i][j + 2] != null)
                {
                    if (gameBoard[i][j + 2].equals(gameBoard[i + 1][j + 1]) && gameBoard[i][j+2].equals(gameBoard[i + 2][j]))
                    {
                        won = true;
                    }
                }
            }
        }
        return won;
    }

    public static void printTheGameBoard(String [][] gameBoard)
    {

        int size = gameBoard.length;

        for (int numberOfRows = 0; numberOfRows < size*2 - 1; numberOfRows++)
        {
            for (int numberOfColumns = 0; numberOfColumns < size*2 - 1; numberOfColumns++)
            {
                if (numberOfRows % 2 == 0)
                {
                    printBoardParts(gameBoard, numberOfRows, numberOfColumns);
                }
                else
                {
                    System.out.print(numberOfColumns % 2 == 0 ? "-" : " ");
                }
            }
            System.out.println();
        }
    }

    public static void printBoardParts(String[][] gameBoard, int numberOfRows, int numberOfColumns)
    {
        int row = numberOfRows/2;
        int column = numberOfColumns/2;

        if (numberOfColumns % 2 == 0)
        {
            if (!checkIfEmpty(gameBoard, row, column))
            {
                System.out.print(gameBoard[row][column]);
            }
            else
            {
                System.out.print(" ");
            }
        } else
        {
            System.out.print("|");
        }
    }

    public static void putSymbolOnBoard (String[][] gameBoard, int row, int column, String symbol)
    {
        gameBoard[row][column] = symbol;
    }

    public static boolean checkIfEmpty (String[][] gameBoard, int row, int column)
    {
        return gameBoard[row][column] == null;
    }

    public static String changeSymbol(String symbol)
    {
        if (symbol.equalsIgnoreCase("X"))
        {
            symbol = "O";
        }
        else
        {
            symbol = "X";
        }

        return symbol;
    }

    public static void splittingCoordinates(String coordinates)
    {
        String[] coordinatesSplitted = coordinates.split(",");
        row = Integer.valueOf(coordinatesSplitted[0])-1;
        column = Integer.valueOf(coordinatesSplitted[1])-1;
    }

    public static boolean checkCoordinates(String coordinates)
    {
        String[] coordinatesSplitted = coordinates.split(",");
        boolean areCoordinatesCorrect = true;

        if (coordinatesSplitted.length != 2)
        {
            areCoordinatesCorrect = false;
        }

        return areCoordinatesCorrect;
    }

    public static boolean checkCoordinates()
    {
        int size = 3;
        boolean areCoordinatesCorrect = true;

        if (row >= size || column >= size || row < 0 || column < 0)
        {
            areCoordinatesCorrect = false;
        }

        return areCoordinatesCorrect;
    }
}
